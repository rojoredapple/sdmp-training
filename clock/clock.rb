class Clock
  attr_accessor :to_s, :hour, :minute

  def initialize(hour, minute)
    @hour = hour
    @minute = minute

    # Accounts for clock rollovers
    if (hour < 0) && (minute < 0)
      hour = 24 - (hour.abs + 1 + minute.abs / 60) % 24
      minute = 60 - minute.abs % 60
    elsif (hour < 0) && (minute >= 0)
      hour = 24 - (hour.abs + minute / 60) % 24
      minute = minute % 60
    elsif (hour >= 0) && (minute < 0)
      hour = (hour - 1 - minute.abs / 60) % 24
      minute = 60 - minute.abs % 60
    elsif (hour >= 24) || (minute >= 60)
      hour = (hour + minute / 60) % 24
      minute = minute % 60
    end

    # Prints the clock input as a string
    if (hour < 10) && (minute < 10)
      time = "0" + "#{hour}" + ":" + "0" + "#{minute}"
    elsif (hour < 10) && (minute >= 10)
      time = "0" + "#{hour}" + ":" + "#{minute}"
    elsif (hour >= 10) && (minute < 10)
      time = "#{hour}" + ":" + "0" + "#{minute}"
    else
      time = "#{hour}" + ":" + "#{minute}"
    end

    @to_s = time
  end


  # Defines method
  def Clock.at(hour, minute)
    Clock.new(hour, minute)
  end

  # Allows for minute addition
  def +(number)
    Clock.at(hour, minute + number)
  end

  # Allows for minute subtraction
  def -(number)
    Clock.at(hour, minute - number)
  end

  # Allows equal clocks to be equal
  def ==(other)
    Clock.at(hour, minute).hour_minute == other.hour_minute
  end

  # Returns array of hour and minute
  def hour_minute
    # Accounts for clock rollovers
    if (@hour < 0) && (@minute < 0)
      [24 - (hour.abs + 1 + minute.abs / 60) % 24, 60 - minute.abs % 60]
    elsif (@hour < 0) && (@minute >= 0)
      [24 - (hour.abs + minute / 60) % 24, minute % 60]
    elsif (@hour >= 0) && (@minute < 0)
      [(hour - 1 - minute.abs / 60) % 24, 60 - minute.abs % 60]
    elsif (@hour >= 24) || (@minute >= 60)
      [(hour + minute / 60) % 24, minute % 60]
    else
      [@hour, @minute]
    end
  end
end


module BookKeeping
  VERSION = 2
end


puts Clock.at(22, 40) == Clock.at(-2, 40)
