class School
  attr_accessor :roster

  def initialize
    @roster = Hash.new {|roster, grade| roster[grade] = []}
  end


  # Allows students to be sorted by grade
  def students(grade)
    roster[grade].sort
  end

  # Allows for a student to be added to the system
  def add(student, grade)
    roster[grade].push(student)
  end

  # Allows for a student to be sorted by grade and alphabetically
  def students_by_grade
    if roster.empty?
      return []
    end
    roster.sort.map {|grade, students| {grade: grade, students: students.sort}}
  end
end


module BookKeeping
  VERSION = 3
end


puts School.new.add('Beemee', 7)
