# SDMP Training - Fall 2018 - Coding Challenge

## Instructions

1. Fork this repository into your account
2. Download the repository to your computer
3. For each exercise, within the relevant directory and open up
   `EXERCISE_test.rb` to review the exercise description and the tests.
4. Then, run `ruby EXERCISE_test.rb` to run the tests.
5. As you get tests to pass, comment out the lines with `skip` on them to enable
   more tests.
6. Repeat until _all_ of the tests pass!

## Required Exercises

1. Clock
2. Grade School

## Extra Credit

1. Poker

These challenges were borrowed from [https://exercism.io](Exercism.io). All
materials &copy; 2018 Exercism, Inc
